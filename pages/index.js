import Head from 'next/head';
import styles from '../styles/Home.module.css';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import EmailOutlined from '@material-ui/icons/EmailOutlined';
import { ThemeProvider } from '@material-ui/styles';
import { createMuiTheme, InputAdornment } from '@material-ui/core';
import ReCAPTCHA from 'react-google-recaptcha';

const theme = createMuiTheme({
  palette: {
    type: "dark"
  }
});

export default function Home() {
  return (
    <div>
      <Head>
        <title>PedoHurt.lol</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <div className={styles.center}>
        <h1>PedoHurt.lol</h1>
        <div className={styles.card}>
          <h1>Check if you've been breached.</h1>
          <form noValidate autoComplete="off">
            <ThemeProvider theme={theme}>
              <TextField
                id="email"
                label="Email"
                variant="outlined"
                required
              />
            </ThemeProvider>
            <div style={{
              marginTop: "20px"
            }} />
            <ReCAPTCHA
              sitekey="6LeV1rgaAAAAAGxE8BNyyjPw5SxdHr3IZF3N6toI"
              theme="dark"
            />
            <div style={{
              marginTop: "20px"
            }} />
            <Button variant="contained" color="primary" disableElevation>
              Check Email
            </Button>
            <p>Or...</p>
          </form>
        </div>
      </div>
    </div>
  )
}
